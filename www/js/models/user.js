/**
 * Defines the User model
 */
(function(w){
  'use strict';

  var User = function(){

    /* Basic user info*/
    this.userName     = m.prop('');
    this.fullName     = m.prop('');
    this.description  = m.prop('');

    /* Basic user interactions */
    this.followers    = Array;
    this.following    = Array;

    /* User actions */
    this.getPosts     = function(){};
    this.publishPost  = function(){};


  };

  return User;

})(window);
