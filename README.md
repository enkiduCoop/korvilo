# Korvilo

Korvilo es un cliente movil para GNUSocial pensado para ser multiplataforma y 
desarrollado con JavaScript.

## Idea

Korvilo pretende ser capaz de interactuar con diferentes instalaciones e 
implementaciones de GNUSocial, pero el desarrollo inicial está basado en comunicar 
con una instlación basada en Quitter.



__Nota__: El proyecto está en pleno desarrollo. No está preparado para ser usado 
en producción.
